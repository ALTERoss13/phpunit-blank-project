<?php

namespace App\Tests\Task2;


use App\Task2\Report\TotalSpentReportCounter\MonthlyExpenses;
use App\Task2\Repository\ExpenseRepository;
use PHPUnit\Framework\TestCase;

class MountlyExpensesTest extends TestCase
{
    public function testSum()
    {
        $expenseRepo = new ExpenseRepository();
        $mountlyExpese = new MonthlyExpenses($expenseRepo);

        $this->assertEquals(7600000, $mountlyExpese->getSum());
    }
}