<?php

namespace App\Tests\Task2;

use App\Task2\Report\TotalSpentReport;
use App\Task2\Report\TotalSpentReportCounter\MonthlyExpenses;
use App\Task2\Report\TotalSpentReportCounter\MonthProduct;
use App\Task2\Repository\ExpenseRepository;
use App\Task2\Repository\SpentProductsRepository;
use PHPUnit\Framework\TestCase;

class TotalSpentReportTest extends TestCase
{
    public function testSum()
    {
        $expenseRepo = new ExpenseRepository();
        $productRepo = new SpentProductsRepository();

        $mountlyExpese = new MonthlyExpenses($expenseRepo);
        $mountlyProduct = new MonthProduct($productRepo);

        $report = new TotalSpentReport();
        $report->add($mountlyExpese);
        $report->add($mountlyProduct);

        $this->assertEquals(9080400, $report->countSum());
    }
}