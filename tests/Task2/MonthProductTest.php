<?php

namespace App\Tests\Task2;


use App\Task2\Report\TotalSpentReportCounter\MonthProduct;
use App\Task2\Repository\SpentProductsRepository;
use PHPUnit\Framework\TestCase;

class MonthProductTest extends TestCase
{
    public function testSum()
    {
        $productRepo = new SpentProductsRepository();
        $mountlyProduct = new MonthProduct($productRepo);

        $this->assertEquals(1480400, $mountlyProduct->getSum());
    }
}