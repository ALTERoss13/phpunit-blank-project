<?php

namespace App\Tests\Task1;

use App\Task1\FigureCollection;
use App\Task1\Square;
use PHPUnit\Framework\TestCase;

class FigureCollectionTest extends TestCase
{
    public function testSum()
    {
        $collection = new FigureCollection();
        $this->assertEquals(0, $collection->countAreaSum());

        $collection->add(new Square(1));
        $this->assertEquals(1, $collection->countAreaSum());
    }
}