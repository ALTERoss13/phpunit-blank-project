<?php

namespace App\Task1;

interface Figure
{
    public function getArea(): float;
}