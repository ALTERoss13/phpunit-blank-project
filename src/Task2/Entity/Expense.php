<?php

namespace App\Task2\Entity;

class Expense
{
    public function __construct(
        public string $name,
        public int $amount,
    )
    {
    }
}
