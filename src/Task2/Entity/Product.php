<?php

namespace App\Task2\Entity;

class Product
{
    public function __construct(
        public string $name,
        public float $quantity,
        public int $price,
    )
    {
    }
}
