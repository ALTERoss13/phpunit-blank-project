<?php

namespace App\Task2\Report;

use App\Task2\Report\TotalSpentReportCounter\TotalSpentReportCounterInterface;

class TotalSpentReport
{
    public $counters = [];

    public function add(TotalSpentReportCounterInterface $counter)
    {
        $this->counters[] = $counter;
    }

    public function countSum()
    {
        $sum = 0;

        foreach ($this->counters as $counter) {
            $sum += $counter->getSum();
        }

        return $sum;
    }
}
