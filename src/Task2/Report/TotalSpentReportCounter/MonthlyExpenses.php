<?php

namespace App\Task2\Report\TotalSpentReportCounter;


use App\Task2\Repository\ExpenseRepository;

class MonthlyExpenses implements TotalSpentReportCounterInterface
{
    private ExpenseRepository $repo;

    public function __construct(ExpenseRepository $repo)
    {
        $this->repo = $repo;
    }

    public function getSum(): float
    {
        $sumExpenseMonth = 0;
        foreach ($this->repo->getItems() as $expense) {
            $sumExpenseMonth += $expense->amount;
        }

        return $sumExpenseMonth;
    }
}