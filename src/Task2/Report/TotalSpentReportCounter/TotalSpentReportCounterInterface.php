<?php

namespace App\Task2\Report\TotalSpentReportCounter;

interface TotalSpentReportCounterInterface
{
    public function getSum():float;
}