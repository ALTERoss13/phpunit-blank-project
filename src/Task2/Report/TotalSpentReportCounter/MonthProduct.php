<?php

namespace App\Task2\Report\TotalSpentReportCounter;

use App\Task2\Repository\SpentProductsRepository;

class MonthProduct implements TotalSpentReportCounterInterface
{
    public function __construct(private SpentProductsRepository $repo)
    {
    }

    public function getSum(): float
    {
        $sumProductMonth = 0;

        foreach ($this->repo->getItems() as $product) {
            $sumProductMonth += $product->quantity * $product->price;
        }

        return $sumProductMonth;
    }
}