<?php

namespace App\Task2\Repository;

use App\Task2\Entity\Product;

class SpentProductsRepository
{
    public function getItems()
    {
        return [
            new Product("Колбаса", 12.6,  60000),
            new Product("Мука", 26,  9400),
            new Product("Соус томатный", 4,  120000),
        ];
    }
}


