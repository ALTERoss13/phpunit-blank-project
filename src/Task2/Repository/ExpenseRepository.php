<?php

namespace App\Task2\Repository;

use App\Task2\Entity\Expense;

class ExpenseRepository
{
    public function getItems()
    {
        return [
            new Expense("Аренда помещения", 5000000),
            new Expense("Электричество", 2600000),
        ];
    }
}
