<?php

namespace App\Task4\Entity;

class Expense
{
    public function __construct(
        public string $name,
        public string $amount,
    )
    {
    }
}
