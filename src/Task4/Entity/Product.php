<?php

namespace App\Task4\Entity;

class Product
{
    public function __construct(
        public string $name,
        public float $quantity,
        public int $price,
    )
    {
    }
}
