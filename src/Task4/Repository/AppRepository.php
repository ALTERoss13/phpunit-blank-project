<?php

namespace App\Task4\Repository;

use App\Task4\Entity\Expense;
use App\Task4\Entity\Product;

class AppRepository
{
    public function getItems()
    {
        return [
            new Expense("Аренда помещения", 5000000),
            new Expense("Электричество", 2600000),
            new Product("Колбаса", 12.6,  60000),
            new Product("Мука", 26,  9400),
            new Product("Соус томатный", 4,  120000),
        ];
    }
}
