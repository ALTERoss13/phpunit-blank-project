<?php

namespace App\Task4\Report;

use App\Task4\Entity\Expense;
use App\Task4\Entity\Product;
use App\Task4\Repository\AppRepository;

class TotalSpentReport
{
    public function __construct(private AppRepository $repo)
    {
    }

    public function getAmount(): float {
        $items = $this->repo->getItems();

        foreach ($items as $item) {
            if ($item instanceof Expense) {
                //
            } elseif ($item instanceof Product) {
                //
            }
        }

        return 0;
    }
}
